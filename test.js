

function addNumbers(a, b, anotherFunction) {
    let z = a + b;
    return anotherFunction(z);
}

addNumbers(1, 4, (x) => {
    return x * x;
});

// function square(x) {
//     return x * x;
// }